name := """PaperScissorRock"""

version := "1.0"

scalaVersion in ThisBuild := "2.12.3"

libraryDependencies ++= Seq(
	"org.scalatest" %% "scalatest" % "3.0.1" % "test"
)

ensimeScalaVersion in ThisBuild := "2.12.3"

ensimeIgnoreMissingDirectories := true

