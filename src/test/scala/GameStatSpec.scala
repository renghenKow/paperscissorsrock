package com.gamigo.paperRockScissors

import org.scalatest.{Entry, FlatSpec, Matchers}

class GameStatSpec extends FlatSpec with Matchers {

  val playerAname = "Player A"
  val playerBname = "Player B"
  val numberOfGames = 30

  //game simulation for 30 moves
  val playerA = SimulationUtils.generatePlayerMoves(playerAname)(numberOfGames) { Paper }
  val generate10PlayerB = SimulationUtils.generatePlayerMoves(playerBname)(10)_ //generator for 10 playerB
  val playerB = generate10PlayerB { Paper } ++ generate10PlayerB(Rock) ++ generate10PlayerB(Scissors)
  val playerBGameObjects: Map[GameObject, Array[Player]] = playerB.groupBy(f => f.gameObject)
  val gameStatAvB = GameStat.evaluateStat(playerA, playerB)
  val gameStatBvA = GameStat.evaluateStat(playerB, playerA)

  "playerA" should "30 move only" in {
    assert(playerA.length == numberOfGames)
  }

  "playerA" should "30 move only and of type Paper" in {
    playerA.map(_.gameObject) should contain only Paper
  }

  "playerA" should "name PlayerA only" in {
    playerA.map(_.name) should contain only playerAname
  }

  "playerB" should "have objects Paper, Rock, Scissors" in {
    playerBGameObjects.keys.toList should contain allOf (Rock, Paper, Scissors)
  }

  "playerB" should "have objects 10 Paper" in {
    playerBGameObjects(Paper) should have length 10
  }

  "playerB" should "have objects 10 Rock" in {
    playerBGameObjects(Rock) should have length 10
  }

  "playerB" should "have objects 10 Scissors" in {
    playerBGameObjects(Scissors) should have length 10
  }

  // test for player A Vs player B 
  "playerA vs PlayerB" should "have 10 Ties" in {
    gameStatAvB(Tie) should have length 10
  }

  "playerA vs PlayerB" should "with PlayerA having 10 wins" in {
    //PlayerA(0) is for playerA Paper
    gameStatAvB(Win(playerA(0))) should have length 10
  }

  "playerA vs PlayerB" should "with PlayerB having 10 wins" in {
    //PlayerB(20) is for playerB Scissors
    gameStatAvB(Win(playerB(20))) should have length 10
  }

  // test for player B Vs player A 
  "playerB vs PlayerA" should "have 10 Ties" in {
    gameStatBvA(Tie) should have length 10
  }

  "playerB vs PlayerA" should "with PlayerA having 10 wins" in {
    //PlayerA(0) is for playerA Paper
    gameStatBvA(Win(playerA(0))) should have length 10
  }

  "playerB vs PlayerA" should "with PlayerB having 10 wins" in {
    //PlayerB(20) is for playerB Scissors
    gameStatBvA(Win(playerB(20))) should have length 10
  }

  "'playerA vs PlayerB' results" should " be identical to 'playerB vs PlayerA' results" in {
    gameStatAvB should be equals gameStatBvA
  }

  val playerA_vs_playerB = """Player A wins 10 of 30 games
Player B wins 10 of 30 games
Tie: 10 of 30 games
No Winner"""

  "game stat summary for 'playerA vs PlayerB' " should s"as follows \n$playerA_vs_playerB" in {
    val summary = GameStat.displayStats(gameStatBvA, numberOfGames, playerAname, playerBname)
    assert(summary == playerA_vs_playerB)
  }

  import SimulationUtils._
  val playerB30RandomMoves = generatePlayerMoves(playerBname)(numberOfGames) {
    numberTomove(generateRandomMove())
  }

  val gameStatAvBrandom30 = GameStat.evaluateStat(playerA, playerB30RandomMoves)
  val gameStatSingleAvBrandom30 = GameStat.evaluateStatWithOnePlayerValue(playerA.head, playerB30RandomMoves)
  "game result evaluateStat for 'playerA vs playerB30RandomMoves' " should "same evaluateStatWithOnePlayerValue" in {
    gameStatAvBrandom30 should be equals gameStatSingleAvBrandom30
  }


  //memoization test
  val generate10PlayerBMem = SimulationUtils.generatePlayerMovesMemoize(playerBname)(10)_ //generator for 10 playerB
  val playerBMem = generate10PlayerBMem { Paper } ++ generate10PlayerBMem(Rock) ++ generate10PlayerBMem(Scissors)
  val playerBGameObjectsMem: Map[GameObject, Array[Player]] = playerB.groupBy(f => f.gameObject)
  val gameStatAvBMem = GameStat.evaluateStat(playerA, playerBMem)

  "playerBMem" should "have objects Paper, Rock, Scissors" in {
    playerBGameObjectsMem.keys.toList should contain allOf (Rock, Paper, Scissors)
  }

  "playerBMem" should "have objects 10 Paper" in {
    playerBGameObjectsMem(Paper) should have length 10
  }

  "playerBMem" should "have objects 10 Rock" in {
    playerBGameObjectsMem(Rock) should have length 10
  }

  "playerBMem" should "have objects 10 Scissors" in {
    playerBGameObjectsMem(Scissors) should have length 10
  }

  // test for player A Vs player B 
  "playerA vs PlayerBMem" should "have 10 Ties" in {
    gameStatAvBMem(Tie) should have length 10
  }

   "playerA vs PlayerBMem" should "with PlayerA having 10 wins" in {
    //PlayerA(0) is for playerA Paper
    gameStatAvBMem(Win(playerA(0))) should have length 10
  }

  "playerA vs PlayerBMem" should "with PlayerB having 10 wins" in {
    //PlayerB(20) is for playerB Scissors
    gameStatAvBMem(Win(playerB(20))) should have length 10
  }

  "game stat summary for 'playerA vs PlayerBMem' " should s"as follows \n$playerA_vs_playerB" in {
    val summary = GameStat.displayStats(gameStatAvBMem, numberOfGames, playerAname, playerBname)
    assert(summary == playerA_vs_playerB)
  }

  //random moves
   val playerB30RandomMovesMem = generatePlayerMovesMemoize(playerBname)(numberOfGames) {
    numberTomove(generateRandomMove())
  }

  val gameStatAvBrandom30Mem = GameStat.evaluateStat(playerA, playerB30RandomMovesMem)
  val gameStatSingleAvBrandom30Mem = GameStat.evaluateStatWithOnePlayerValue(playerA.head, playerB30RandomMovesMem)
  "game result evaluateStat for 'playerA vs playerB30MemRandomMoves' " should "same evaluateStatWithOnePlayerValue" in {
    gameStatAvBrandom30Mem should be equals gameStatSingleAvBrandom30Mem
  }
 

}
