package com.gamigo.paperRockScissors

import org.scalatest.{FlatSpec, Matchers}

class RulesSpec extends FlatSpec with Matchers {

  //player A
  val playerA1 = Player("A - with Paper", Paper)
  val playerA2 = Player("A - with Rock", Rock)
  val playerA3 = Player("A - with Scissors", Scissors)

  //Player B
  val playerB1 = Player("B - with Paper", Paper)
  val playerB2 = Player("B - with Rock", Rock)
  val playerB3 = Player("B - with Scissors", Scissors)

  //playerA1 vs all possibiliteis of playerB
  "playerA1 vs PlayerB1" should "a tie" in {
    val result = GameRules.winOrLoss(playerA1, playerB1)
    assert(result == Tie)
  }

  "playerA1 vs PlayerB2" should "a win for playerA1" in {
    val result = GameRules.winOrLoss(playerA1, playerB2)
    assert(result == Win(playerA1))
  }

  "playerA1 vs PlayerB3" should "a win for playerB3" in {
    val result = GameRules.winOrLoss(playerA1, playerB3)
    assert(result == Win(playerB3))
  }

  //playerA2 vs all possibiliteis of playerB
  "playerA2 vs PlayerB1" should "a win for player B1" in {
    val result = GameRules.winOrLoss(playerA2, playerB1)
    assert(result == Win(playerB1))
  }

  "playerA2 vs PlayerB2" should "a tie" in {
    val result = GameRules.winOrLoss(playerA2, playerB2)
    assert(result == Tie)
  }

  "playerA2 vs PlayerB3" should "a win for playerA2" in {
    val result = GameRules.winOrLoss(playerA2, playerB3)
    assert(result == Win(playerA2))
  }

  //playerA3 vs all possibiliteis of playerB
  "playerA3 vs PlayerB1" should "a win for playerA3" in {
    val result = GameRules.winOrLoss(playerA3, playerB1)
    assert(result == Win(playerA3))
  }

  "playerA3 vs PlayerB2" should "a tie" in {
    val result = GameRules.winOrLoss(playerA3, playerB2)
    assert(result == Win(playerB2))
  }

  "playerA3 vs PlayerB3" should "a win for playerA2" in {
    val result = GameRules.winOrLoss(playerA3, playerB3)
    assert(result == Tie)
  }
}
