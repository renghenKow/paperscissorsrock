package com.gamigo.paperRockScissors

import org.scalatest.{FlatSpec, Matchers}

class SimulationUtilsSpec extends FlatSpec with Matchers {

  val playerAOneMove = SimulationUtils.generatePlayerMoves("PlayerA")(1){Paper}

  //playerA 
  "playerA" should "1 move only" in {
    assert(playerAOneMove.length == 1)
  }

  "playerA" should "1 move only and of type Paper" in {
    playerAOneMove.map(_.gameObject) should contain only Paper
  }

  "playerA" should "name PlayerA" in {
    playerAOneMove.map(_.name) should contain only "PlayerA"
  }

  val playerBTenMove = SimulationUtils.generatePlayerMoves("PlayerB")(10){Rock} 
  //playxerB 
  "playerB" should "10 move only" in {
    assert(playerBTenMove.length == 10)
  }

  "playerB" should "10 move only and of type Rock" in {
    playerBTenMove.map(_.gameObject) should contain only Rock
  }

  "playerB" should "name PlayerB" in {
    playerBTenMove.map(_.name) should contain only "PlayerB"
  }

}
