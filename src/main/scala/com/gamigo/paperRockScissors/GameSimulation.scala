package com.gamigo.paperRockScissors

/**
 * @author renghen
 * main game simulation using batch system
 */
object GameSimulation {

  def main(args: Array[String]): Unit = {
    import SimulationUtils._

    val playerAname = "Player A"
    val playerBname = "Player B"
    val numberOfGames = 100

    val playerA100Moves = generatePlayerMoves(playerAname)(numberOfGames)(Paper)
    val playerB100Moves = generatePlayerMoves(playerBname)(numberOfGames) {
      numberTomove(generateRandomMove())
    }
    val playerB100MovesMem = generatePlayerMoves(playerBname)(numberOfGames) {
      numberTomove(generateRandomMove())
    }

    //collection vs collection
    val resultStats = GameStat.evaluateStat(playerA100Moves, playerB100Moves)
    val displayStats = GameStat.displayStats(resultStats, numberOfGames, playerAname, playerBname)
    println(displayStats)

    /*
    println("*" * 25) // line seperator

    //single value vs collection
    val gameStatSingleAvBrandom100 = GameStat.evaluateStatWithOnePlayerValue(playerA100Moves.head, playerB100Moves)
    val displayStatsOfSingleAvBrandom100 = GameStat.displayStats(gameStatSingleAvBrandom100, numberOfGames, playerAname, playerBname)
    println(displayStatsOfSingleAvBrandom100)
     */

    /*
    //same thing as above except with memoization added
    println("*" * 25) // line seperator

    // collection vs collection memoised
    val resultStatsMem = GameStat.evaluateStat(playerA100Moves, playerB100MovesMem)
    val displayStatsMem = GameStat.displayStats(resultStatsMem, numberOfGames, playerAname, playerBname)
    println(displayStatsMem)
     */

    /*
    println("*" * 25) // line seperator

    //single value vs collection mem
    val gameStatSingleAvBrandom100Mem = GameStat.evaluateStatWithOnePlayerValue(playerA100Moves.head, playerB100MovesMem)
    val displayStatsOfSingleAvBrandom100Mem = GameStat.displayStats(gameStatSingleAvBrandom100Mem, numberOfGames, playerAname, playerBname)
    println(displayStatsOfSingleAvBrandom100Mem)

     */
  }

}
