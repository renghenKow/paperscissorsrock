package com.gamigo.paperRockScissors

object GameRules{

  def winOrLoss(p1 : Player , p2 :Player) : WinLoss = {
    p1.gameObject match {
      case Paper => p2.gameObject match{
        case Paper => Tie
        case Rock => Win(p1)
        case Scissors => Win(p2)
      }

      case Rock => p2.gameObject match{
        case Paper => Win(p2)
        case Rock => Tie
        case Scissors => Win(p1)
      }

      case Scissors => p2.gameObject match{
        case Paper => Win(p1)
        case Rock => Win(p2)
        case Scissors => Tie
      }
    }
  }

}
