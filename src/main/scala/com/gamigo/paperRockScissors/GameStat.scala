package com.gamigo.paperRockScissors

/**
 * @author renghen
 * module to contain functions for game statistics
 */

object GameStat {

  /**
    * evaluate with 2 different collections of players
    * where there can be different values on both sides
    */
  def evaluateStat(player1 :Array[Player], player2 : Array[Player]): Map[WinLoss, Array[WinLoss]] = {
    val zipPlayers = player1 zip player2
    val wins: Array[WinLoss] = zipPlayers.map{ case (p1,p2) => 
      GameRules.winOrLoss(p1, p2)
    }
    wins.groupBy(el => el)
  }

  /**
    * evaluate with 1 player vs a collections of player values
    * the 1st player will always have a fixed value 
    */
  def evaluateStatWithOnePlayerValue(player1 : Player, player2 : Array[Player]): Map[WinLoss, Array[WinLoss]] = {    
    val wins: Array[WinLoss] = player2.map{ p2 => 
      GameRules.winOrLoss(player1, p2)
    }
    wins.groupBy(el => el)
  }

  def displayStats(result: Map[WinLoss, Array[WinLoss]], numberOfGames: Int, player1: String, player2: String) = {
    val keys = result.keys.toList
    val winners: List[Win] = keys.collect { case win: Win => win }
    val winA = winners.filter { win => win.player.name == player1 }
    val winB = winners.filter { win => win.player.name == player2 }
    val winACount = result(winA.head).length
    val winBCount = result(winB.head).length


    val playerADisplay = s"$player1 wins $winACount of $numberOfGames games\n"
    val playerBDisplay = s"$player2 wins $winBCount of $numberOfGames games\n"
    val tieDisplay = s"Tie: ${result(Tie).length} of $numberOfGames games\n"
    val winnerDisplay = if (winACount > winBCount)  s"Winner is: ${winA.head.player.name} ($winACount to $winBCount wins)"
    else if (winACount < winBCount)  s"Winner is:  ${winB.head.player.name} ($winBCount to $winACount wins)"
    else "No Winner"

    playerADisplay + playerBDisplay + tieDisplay + winnerDisplay
  }

}
