package com.gamigo.paperRockScissors

/**
 * @author renghen
 * module to contain functions for simulation
 * and generate moves
 */

import scala.util.Random

object SimulationUtils {

  val randomGen = Random
  val numberOfMoves = 3

  def generateRandomMove(): Byte = randomGen.nextInt(numberOfMoves).toByte

  def numberTomove(moveNumber: Byte): GameObject = moveNumber match {
    case 0 => Paper
    case 1 => Rock
    case 2 => Scissors
  }

  def generatePlayerMoves(playerName: String)(numberOfMoves: Int)(gen: => GameObject): Array[Player] = {
    (for (_ <- 0 until numberOfMoves)
      yield Player(playerName, gen)).toArray
  }

  /**
   * another way to create list of players
   * It may be more performant as it uses memoisation
   */
  def generatePlayerMovesMemoize(playerName: String)(numberOfMoves: Int)(gen: => GameObject): Array[Player] = {
    val allMoves = Map(
      Paper -> Player(playerName, Paper),
      Rock -> Player(playerName, Rock),
      Scissors -> Player(playerName, Scissors)
    )

    (for (_ <- 0 until numberOfMoves) yield {
      allMoves(gen)
    }).toArray
  }

  //generatePlayerMoves(_ :String)(100)(Paper)




  def quickNdirtySimulation(numberOfGames : Int) = {
    val result = Array.fill(numberOfGames)(randomGen.nextInt(3).toByte)
      .groupBy{ game => game}

    //assume that paper = 0, rock =1 scissors = 2
    val winACount = result(1).length
    val winBCount = result(2).length
    val playerADisplay = s"Player A wins $winACount of $numberOfGames games\n"
    val playerBDisplay = s"Playe B wins  $winBCount of $numberOfGames games\n"
    val tieDisplay = s"Tie: ${result(0).length} of $numberOfGames games\n"
    val winnerDisplay = if (winACount > winBCount)  s"Winner is: Player A ($winACount to $winBCount wins)"
    else if (winACount < winBCount)  s"Winner is:  Player B ($winBCount to $winACount wins)"
    else "No Winner"

    playerADisplay + playerBDisplay + tieDisplay + winnerDisplay
  }

}
