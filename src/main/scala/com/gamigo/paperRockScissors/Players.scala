package com.gamigo.paperRockScissors

// defines game object
sealed trait GameObject
object Paper extends GameObject
object Rock  extends GameObject
object Scissors extends GameObject

// defining a player
case class Player(name : String, gameObject : GameObject)

//defining a win
sealed trait WinLoss
case class Win(player : Player) extends WinLoss
case class Loss(player : Player) extends WinLoss
object Tie extends WinLoss
